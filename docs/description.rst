==================
*YAML* data reader
==================

*galactic-io-data-yaml* is a
`YAML <https://en.wikipedia.org/wiki/YAML>`_
data reader plugin for **GALACTIC**.

It uses the ``yaml.safe_load`` function of the `PyYAML <https://pyyaml.org/>`_
library.

The file extension is ``.yaml`` or ``.yml``. The individuals are represented
either by:

* a list: individuals are named by an integer starting from 0;
* a dictionary: individuals are named by their keys.

For example:

.. code-block:: yaml
    :class: admonition

    0: [c, e, s]
    1: [o, s]
    2: [e, p]
    3: [o, p]
    4: [c, e, s]
    5: [o, p]
    6: [c, e]
    7: [o, p]
    8: [c, e]
    9: [c, o, s]

