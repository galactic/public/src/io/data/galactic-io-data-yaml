"""Population test module."""

from unittest import TestCase

from galactic.io.data.core import PopulationFactory
from galactic.io.data.yaml import YAMLDataReader


class PopulationTest(TestCase):
    def test_population(self):
        self.assertTrue(
            any(
                isinstance(reader, YAMLDataReader)
                for reader in PopulationFactory.readers()
            ),
        )
