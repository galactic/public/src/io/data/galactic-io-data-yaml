"""
YAML Data reader.
"""

from collections.abc import Iterator, Mapping
from typing import TextIO, cast

import yaml
from galactic.helpers.core import default_repr
from galactic.io.data.core import PopulationFactory


# pylint: disable=too-few-public-methods
class YAMLDataReader:
    """
    `̀YAML`̀ Data reader.

    Example
    -------
    >>> from pprint import pprint
    >>> from galactic.io.data.yaml import YAMLDataReader
    >>> reader = YAMLDataReader()
    >>> import io
    >>> data = '''# This is a YAML document.
    ... - name: Galois
    ...   firstname: Évariste
    ... - name: Wille
    ...   firstname: Rudolf
    ... '''
    >>> individuals = reader.read(io.StringIO(data))
    >>> pprint(individuals)
    {'0': {'firstname': 'Évariste', 'name': 'Galois'},
     '1': {'firstname': 'Rudolf', 'name': 'Wille'}}

    """

    __slots__ = ()

    def __repr__(self) -> str:
        return cast(str, default_repr(self))

    @classmethod
    def read(cls, data_file: TextIO) -> Mapping[str, object]:
        """
        Read a ``YAML`` data file.

        Parameters
        ----------
        data_file
            A readable text file.

        Returns
        -------
        Mapping[str, object]
            The data.

        """
        individuals = yaml.safe_load(data_file)
        if isinstance(individuals, Mapping):
            return individuals
        return {str(index): value for index, value in enumerate(individuals)}

    @classmethod
    def extensions(cls) -> Iterator[str]:
        """
        Get an iterator over the supported extensions.

        Returns
        -------
        Iterator[str]
            An iterator over the supported extensions

        """
        return iter([".yaml", ".yml"])


def register() -> None:
    """
    Register an instance of a ``yaml`` data reader.
    """
    PopulationFactory.register_reader(YAMLDataReader())
