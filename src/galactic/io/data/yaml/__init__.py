"""
YAML Data reader.
"""

from ._main import YAMLDataReader, register

__all__ = ("YAMLDataReader", "register")
